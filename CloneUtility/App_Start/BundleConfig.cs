﻿using System.Web;
using System.Web.Optimization;

namespace CloneUtility
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js", "~/Scripts/respond.js", "~/Scripts/toastr.js", "~/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/ng").Include("~/Scripts/jquery.min.js", "~/Scripts/angular.min.js", "~/Scripts/angular-route.min.js", "~/Scripts/loading-bar.min.js"));

            #region Custom Scripts

            AddNonMinifiedBundle("~/bundles/app-angularapp", "~/Scripts/app/angularapp.js", ref bundles);
            AddNonMinifiedBundle("~/bundles/app-admin", "~/Scripts/app/admin.js", ref bundles);

            #endregion
        }

        private static void AddNonMinifiedBundle(string newName, string oldName, ref BundleCollection bundles)
        {
            var bundle = new ScriptBundle(newName).Include(oldName);
            bundle.Transforms.Clear();
            bundles.Add(bundle);
        }
    }
}

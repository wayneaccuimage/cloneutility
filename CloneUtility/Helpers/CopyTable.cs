﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloneUtility.Helpers
{
    public class CopyTable
    {
        private static ILog _logger = LogManager.GetLogger(typeof(CopyTable));

        //copy table data from source to destination
        public static void CopyTableData(IDbConnection source, IDbConnection destination, String sourceSQL, String destinationTableName)
        {
            IDbCommand cmd = source.CreateCommand();
            cmd.CommandText = sourceSQL;
            
            try
            {
                source.Open();
                destination.Open();
                IDataReader rdr = cmd.ExecuteReader();
                DataTable schemaTable = rdr.GetSchemaTable();

                IDbCommand insertCmd = destination.CreateCommand();
                string paramsSQL = String.Empty;

                //build the insert statement
                foreach (DataRow row in schemaTable.Rows)
                {
                    if (paramsSQL.Length > 0)
                        paramsSQL += ", ";
                    paramsSQL += "@" + row["ColumnName"].ToString();

                    IDbDataParameter param = insertCmd.CreateParameter();
                    param.ParameterName = "@" + row["ColumnName"].ToString();
                    param.SourceColumn = row["ColumnName"].ToString();

                    if (row["DataType"] == typeof(System.DateTime))
                    {
                        param.DbType = DbType.DateTime;
                    }

                    insertCmd.Parameters.Add(param);
                }
                insertCmd.CommandText = String.Format("insert into {0} ( {1} ) values ( {2} )", destinationTableName, paramsSQL.Replace("@", String.Empty), paramsSQL);
                int counter = 0;
                int errors = 0;
                while (rdr.Read())
                {
                    try
                    {
                        foreach (IDbDataParameter param in insertCmd.Parameters)
                        {
                            object col = rdr[param.SourceColumn];
                            
                            if (param.DbType == DbType.DateTime)
                            {
                                if (col != DBNull.Value)
                                {
                                    //sql server can not have dates less than 1753
                                    if (((DateTime)col).Year < 1753)
                                    {
                                        param.Value = DBNull.Value;
                                        continue;
                                    }
                                }
                            }

                            param.Value = col;
                        }
                        insertCmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (errors == 0)
                            _logger.Debug(ex.Message);
                        errors++;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error msg: " + ex.Message + "; InnerException : " + ex.InnerException);
            }
            finally
            {
                destination.Close();
                source.Close();
            }
        }
    }
}

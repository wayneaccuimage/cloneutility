﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CloneUtility.Helpers
{
    public class DataBaseHelper
    {
        public static string connectionString(string dbName)
        {
            var connectionString = "server=" + System.Configuration.ConfigurationManager.AppSettings["Server"] + "; port= " + System.Configuration.ConfigurationManager.AppSettings["Port"] + ";username=" + System.Configuration.ConfigurationManager.AppSettings["Username"] + "; password=" + System.Configuration.ConfigurationManager.AppSettings["Password"] + ";database=" + dbName;
            return connectionString;
        }

        //Execute MYSQL queries
        public static DataTable ExecuteQuery(string connectionString, string query)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                MySqlCommand command = new MySqlCommand(query, connection);
                
                try
                {
                    connection.Open();
                    DataTable dataTable = new DataTable();

                    MySqlDataReader reader = command.ExecuteReader();
                    dataTable.Load(reader);
                    reader.Close();
                    connection.Close();

                    return dataTable;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        //To get the value of a column
        public static object GetColumnValue(string colName, DataTable table)
        {
            if (table != null && table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];

                foreach (DataColumn col in table.Columns)
                {
                    if (col.ColumnName.ToUpper().Equals(colName.ToUpper()))
                    {
                        return row[col];
                    }
                }
            }

            return null;
        }

        public static object GetFirstFieldValue(DataTable table)
        {
            if (table != null && table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];
                return row[0];
            }

            return null;
        }

        public static object GetFirstFieldValueFromQuery(string connString, string query)
        {
            DataTable table = ExecuteQuery(connString, query);
            return GetFirstFieldValue(table);
        }
    }
}
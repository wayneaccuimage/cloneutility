﻿using CloneUtility.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace CloneUtility.Api
{
    public class ViewIniController : ApiController
    {
        private static ILog _logger = LogManager.GetLogger(typeof(ViewIniController));
        string dServerName;
        string dTargetPath;
        string dTargetFolder;
        string dUserName;
        string dPassword;
        string message;
        List<object> points = new List<object>();
     
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [Route("api/ViewIni/value")]
        public object Post(DestinationDetails value)
        {

            _logger.Info("Getting Details of destination C$\\Program Files (x86)\\AccuImage");
            dTargetFolder = value.dTargetFolder;
            dTargetPath = value.dTargetPath;
            dTargetPath = dTargetPath.Replace("/", "\\");
            dServerName = value.dServerName;
            dUserName = value.dUserName;
            dPassword = value.dPassword;
            var dPath = Path.Combine(dTargetPath, dTargetFolder);
            var path = Path.Combine(dServerName, dPath);

            _logger.Info("Connecting to " + path);
            var impersonationContext = new WrappedImpersonationContext(dServerName, dUserName, dPassword);
            impersonationContext.Enter();
            _logger.Info("Connected");

            try
            {
                _logger.Info("Getting ini file from " + path);
                var fileName = Path.Combine(path, value.selectedFile);
                string file = fileName.ToString();
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
                StreamReader sr = new StreamReader(fs);
                string r = sr.ReadToEnd();
                string[] keys = Regex.Split(r, "\r\n");
                _logger.Info("Reading ini file from " + path);
                foreach (var key in keys)
                {
                    points.Add(key.ToString());
                    _logger.Info("Reading ini value " + key.ToString());
                }
                sr.Close();
                fs.Close();
            }
            catch (Exception e)
            {
                _logger.Error("Error is " + e.Message);
                message = string.Format("Message: {0}", e.Message);
            }
            return new { points, message };

        }

       // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
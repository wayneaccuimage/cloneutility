﻿using System;
using System.Net;
using System.Runtime.InteropServices;

namespace CloneUtility
{
    public class NetworkConnection : IDisposable
        {
            string srcPath;
           private NetworkCredential nCredentials;

        public NetworkConnection(string srcPath, NetworkCredential nCredentials)
        {
            this.srcPath = srcPath;
            this.nCredentials = nCredentials;
       
                var netResource = new NetResource()
                {
                    Scope = ResourceScope.GlobalNetwork,
                    ResourceType = ResourceType.Disk,
                    DisplayType = ResourceDisplaytype.Share,
                    RemoteName = srcPath
                };

                var userName = string.IsNullOrEmpty(nCredentials.Domain)
                    ? nCredentials.UserName
                    : string.Format(@"{0}\{1}", nCredentials.Domain, nCredentials.UserName);

                var result = WNetAddConnection2(
                    netResource,
                    nCredentials.Password,
                    userName,
                    0);

                if (result != 0)
                {
                    throw new System.ComponentModel.Win32Exception(result, "Error connecting to remote share" + nCredentials.Password.ToString() + "--" + nCredentials.Domain + nCredentials.UserName);
                }
            }

        //~NetworkConnection()
        //{
        //    Dispose(false);
        //}

        public void Dispose()
        {
            
            GC.SuppressFinalize(this);
        }

        //protected virtual void Dispose(bool disposing)
        //{
        //    WNetCancelConnection2(srcPath, 0, true);
        //}

        [DllImport("mpr.dll")]
            private static extern int WNetAddConnection2(NetResource netResource,
                string password, string username, int flags);

            [DllImport("mpr.dll")]
            private static extern int WNetCancelConnection2(string name, int flags,
                bool force);
        }

        [StructLayout(LayoutKind.Sequential)]
        public class NetResource
        {
            public ResourceScope Scope;
            public ResourceType ResourceType;
            public ResourceDisplaytype DisplayType;
            public int Usage;
            public string LocalName;
            public string RemoteName;
            public string Comment;
            public string Provider;
        }

        public enum ResourceScope : int
        {
            Connected = 1,
            GlobalNetwork,
            Remembered,
            Recent,
            Context
        };

        public enum ResourceType : int
        {
            Any = 0,
            Disk = 1,
            Print = 2,
            Reserved = 8,
        }

        public enum ResourceDisplaytype : int
        {
            Generic = 0x0,
            Domain = 0x01,
            Server = 0x02,
            Share = 0x03,
            File = 0x04,
            Group = 0x05,
            Network = 0x06,
            Root = 0x07,
            Shareadmin = 0x08,
            Directory = 0x09,
            Tree = 0x0a,
            Ndscontainer = 0x0b
        }
       
    }

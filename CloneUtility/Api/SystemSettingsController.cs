﻿using CloneUtility.Helpers;
using CloneUtility.Models;
using log4net;
using log4net.Appender;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace CloneUtility.Api
{
    public class SystemSettingsController : ApiController
    {
        private static ILog _logger = LogManager.GetLogger(typeof(SystemSettingsController));

        //settings from web.config
        string server = System.Configuration.ConfigurationManager.AppSettings["serverIP"];
        string userID = System.Configuration.ConfigurationManager.AppSettings["MySqlUserID"];
        string password = System.Configuration.ConfigurationManager.AppSettings["MySqlPassword"];
        string db = System.Configuration.ConfigurationManager.AppSettings["database"];
        string port = System.Configuration.ConfigurationManager.AppSettings["port"];


        // GET api/<controller>

        List<object> lines = new List<object>();
        //loading log file 
        [Route("api/SystemSettings/")]
        public object Post()
        {
            try {
                string startupPath = Path.GetDirectoryName(Assembly.GetAssembly(typeof(SystemSettingsController)).CodeBase);
                startupPath = startupPath.Substring(6, startupPath.IndexOf("bin") - 7);
                var Opath = @"" + startupPath + @"\Log\CloneUtility.txt";
                //var Opath = @"C:\inetpub\wwwroot\CloneUtility\Log\CloneUtility.txt";

                int x = 500;   // number of lines you want to get

                var buffor = new Queue<string>(x);

                using (var fs = new FileStream(Opath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (var file = new StreamReader(fs))
                {
                    //var file = new StreamReader(Opath);
                    int lineNumber = 0;
                    while (!file.EndOfStream)
                    {
                        string line = file.ReadLine();
                        lineNumber += 1;

                        if (buffor.Count >= x)
                            buffor.Dequeue();
                        buffor.Enqueue( (lineNumber.ToString()) + "\t: \t" + line);
                    }
                    file.Close();
                };                    

                string[] lastLines = buffor.ToArray();
                
                var startLine = 0;
                if(lastLines.Count() > x)
                {
                    startLine = lastLines.Count() - x;
                }
                for(int i=startLine; i < lastLines.Count(); i++)
                {
                    lines.Add(lastLines[i].ToString());
                }
            }
            catch (Exception ex) {
                var msg = ex.Message;
                lines.Add(msg);
                _logger.Error(ex.Message);
            }
            return lines;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [Route("api/SystemSettings/model")]
        public object Post(DbDestinationDetails model)
        {
            string desIPAddress = model.desIPAddress;
            string desUserName = model.desUsername;
            string desPassword = model.desPassword;
            string desDvsName = model.desDvsName;
            string desDvmName = model.desDbmName;
            string desPort = model.desPort;
            string desFolderPath = model.desFolderPath;

            string sysSettExePath = desFolderPath + "/MakePDF/AccuWrap.exe";
            string sysSettLogPath = desFolderPath + "/MakePDF/log/";

            var connectionString = "server=" + desIPAddress + "; port=" + desPort + ";username=" + desUserName + "; password=" + desPassword;

            //updating _system_settings
            var exePathUpdate = string.Format("UPDATE {0}._system_settings SET SETTING_VALUE = '{1}' WHERE setting_name = '{2}'", desDvsName, sysSettExePath, "CONSOLE_FORMS_DOCUMENT_RETRIEVAL_EXE_PATH");
            var logPathUpdate = string.Format("UPDATE {0}._system_settings SET SETTING_VALUE = '{1}' WHERE setting_name = '{2}'", desDvsName, sysSettLogPath, "CONSOLE_FORMS_DOCUMENT_RETRIEVAL_LOG_PATH");
            var dbidUpdate = string.Format("UPDATE {0}._system_settings SET SETTING_VALUE = '{1}' WHERE setting_name = '{2}'", desDvsName, desDvmName, "CONSOLE_FORMS_DOCUMENT_RETRIEVAL_DBID");
            try {
                DataTable dt = DataBaseHelper.ExecuteQuery(connectionString, exePathUpdate);
                dt = DataBaseHelper.ExecuteQuery(connectionString, logPathUpdate);
                dt = DataBaseHelper.ExecuteQuery(connectionString, dbidUpdate);
            }catch(Exception ex)
            {
                _logger.Error(ex.Message);
            }

            //retrive system_settings values from destination DB
            var exePathQuery = string.Format("SELECT SETTING_VALUE FROM {0}._system_settings where setting_name='{1}'", desDvsName, "CONSOLE_FORMS_DOCUMENT_RETRIEVAL_EXE_PATH");
            var logPathQuery = string.Format("SELECT SETTING_VALUE FROM {0}._system_settings where setting_name='{1}'", desDvsName, "CONSOLE_FORMS_DOCUMENT_RETRIEVAL_LOG_PATH");
            var dbidQuery = string.Format("SELECT SETTING_VALUE FROM {0}._system_settings where setting_name='{1}'", desDvsName, "CONSOLE_FORMS_DOCUMENT_RETRIEVAL_DBID");

            var EXE_PATH = Convert.ToString(DataBaseHelper.GetFirstFieldValueFromQuery(connectionString, exePathQuery) ?? "");

            var LOG_PATH = Convert.ToString(DataBaseHelper.GetFirstFieldValueFromQuery(connectionString, logPathQuery) ?? "");

            var DBID = Convert.ToString(DataBaseHelper.GetFirstFieldValueFromQuery(connectionString, dbidQuery) ?? "");

            var settings = new JObject();
            settings.Add("CONSOLE_FORMS_DOCUMENT_RETRIEVAL_EXE_PATH", EXE_PATH);
            settings.Add("CONSOLE_FORMS_DOCUMENT_RETRIEVAL_LOG_PATH", LOG_PATH);
            settings.Add("CONSOLE_FORMS_DOCUMENT_RETRIEVAL_DBID", DBID);

            //loading complete _system_settings table and saving them to kvpList
            var completeTableQuery = string.Format("select * from {0}._system_settings", desDvsName);
            DataTable sysSettings = DataBaseHelper.ExecuteQuery(connectionString, completeTableQuery);

            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string, string>>();

            if (sysSettings != null && sysSettings.Rows.Count > 0)
            {
                foreach (DataRow row in sysSettings.Rows)
                {
                    var name = row.Field<string>("SETTING_NAME");
                    var value = row.Field<string>("SETTING_VALUE");
                    KeyValuePair<string, string> newEntry = new KeyValuePair<string, string>(name, value);
                    kvpList.Add(newEntry);
                }
            }

            //getting the settingNames saved in the _clone_utility database
            try
            {
                var defaultServerConnection = "server=" + server + "; port=" + port + ";username=" + userID + "; password=" + password;
                string query = string.Format("select * from {0}._system_settings", db);

                var settingFromEntity = DataBaseHelper.ExecuteQuery(defaultServerConnection, query);

                //var settingFromEntity = context.C_system_settings.ToArray();
                return new { settings, kvpList, settingFromEntity };
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
            List<string> settFromEntity = new List<string>();
            return new { settings, kvpList, settFromEntity };
        }

        //adding new setting Name to _clone_utility database
        [Route("api/SystemSettings/addSett/{key}")]
        public IHttpActionResult post(string key)
        {
            //clone_utilityEntities context = new clone_utilityEntities();

            try
            {
                //C_system_settings settingName = new C_system_settings();
                //settingName.SETTING_NAME = key;
                //context.C_system_settings.Add(settingName);
                //context.SaveChanges();

                var defaultServerConnection = "server=" + server + "; port=" + port + ";username=" + userID + "; password=" + password;
                string query = string.Format("INSERT INTO {0}._system_settings ( SETTING_NAME ) VALUES ('{1}');", db, key);

                var queryExecute = DataBaseHelper.ExecuteQuery(defaultServerConnection, query);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return InternalServerError();
            }
        }

        //save settings to destination's dvstream database
        // PUT api/<controller>/
        [Route("api/SystemSettings")]
        public object Put([FromBody]DbDestinationDetails model)
        {
            string desIPAddress = model.desIPAddress;
            string desUserName = model.desUsername;
            string desPassword = model.desPassword;
            string desDvsName = model.desDvsName;
            List<string> desDBNames = new List<string>()/*model.desDBName.Split(',')*/;
            string desPort = model.desPort;

            var connectionString = "server=" + desIPAddress + "; port=" + desPort + ";username=" + desUserName + "; password=" + desPassword;

            List<object> setts = model.sysSettings;

            _logger.Info("updating _system_settings of database: " + desDvsName);
            foreach (object kvp in setts)
            {
                if (kvp != null)
                {
                    JToken jToken = (JToken)kvp;
                    var key = jToken.Value<string>("Key");
                    var value = jToken.Value<string>("Value");
                    value = value.Replace("'", "\"");
                    try
                    {
                        var query = string.Format("UPDATE {0}._system_settings SET SETTING_VALUE = '{1}' WHERE SETTING_NAME = '{2}'", desDvsName, value, key);
                        DataTable table = DataBaseHelper.ExecuteQuery(connectionString, query);
                        _logger.Info("updated system_settings SETTING_VALUE of: " + key);
                    }
                    catch(Exception ex)
                    {
                        _logger.Error("update system_settings SETTING_VALUE of: " + key + "failed. \n" + ex.Message);
                        var query = string.Format("INSERT INTO {0}._system_settings (SETTING_NAME,SETTING_VALUE) VALUES ('{1}','{2}')", desDvsName, key, value);
                        DataTable table = DataBaseHelper.ExecuteQuery(connectionString, query);
                        return BadRequest();
                    }
                }
            }
            _logger.Info("******************************system_settings updated**********************************");

            //reloading settingValues from systemsettings
            var completeTableQuery = string.Format("select * from {0}._system_settings", desDvsName);
            DataTable sysSettings = DataBaseHelper.ExecuteQuery(connectionString, completeTableQuery);

            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string, string>>();

            if (sysSettings != null && sysSettings.Rows.Count > 0)
            {
                foreach (DataRow row in sysSettings.Rows)
                {
                    var name = row.Field<string>("SETTING_NAME");
                    var value = row.Field<string>("SETTING_VALUE");
                    KeyValuePair<string, string> newEntry = new KeyValuePair<string, string>(name, value);
                    kvpList.Add(newEntry);
                }
            }
            //reloading settingNames from the _clone_utility database
            try
            {
                var defaultServerConnection = "server=" + server + "; port=" + port + ";username=" + userID + "; password=" + password;
                string query = string.Format("select * from {0}._system_settings", db);

                var settingFromEntity = DataBaseHelper.ExecuteQuery(defaultServerConnection, query);

                //var settingFromEntity = context.C_system_settings.ToArray();
                return new { kvpList, settingFromEntity };
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            List<string> settFromEntity = new List<string>();
            return new { kvpList, settFromEntity };
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
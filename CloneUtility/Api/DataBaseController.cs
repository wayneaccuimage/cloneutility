﻿using CloneUtility.Helpers;
using CloneUtility.Models;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CloneUtility.Api
{
    public class DataBaseController : ApiController
    {
        private static ILog _logger = LogManager.GetLogger(typeof(DataBaseController));
        string srcIPAddress;
        string srcUserName;
        string srcPassword;
        List<String> srcDbNames = new List<String>();
        string srcPort;
        string desIPAddress;
        string desUserName;
        string desPassword;
        List<String> desDBNames = new List<String>();
        string desPort;
        string desTableName;
        string message = string.Empty;
        List<String> sourceDBNames = new List<String>();

        List<String> rows = new List<String>();
        List<String> tableNames1 = new List<String>();
        List<String> viewNames1 = new List<String>();
        List<String> tableNames2 = new List<String>();
        List<String> viewNames2 = new List<String>();


        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        //POST (to verify MYSQL connection)
        [Route("api/DataBase/verify")]
        public IHttpActionResult PostVerify(DbDestinationDetails desDetails)
        {
            var connectionString = "server=" + desDetails.desIPAddress + "; port=" + desDetails.desPort + ";username=" + desDetails.desUsername + "; password=" + desDetails.desPassword;
            _logger.Info("Verifying mysql connection for: " + desDetails.desIPAddress);

            MySqlConnection connection = new MySqlConnection(connectionString);

            try
            {
                connection.Open();
                //connection successful return 200
                _logger.Info("**********************connection successful!*****************************");
                connection.Close();
                return Ok();
            } catch(Exception ex)
            {
                _logger.Error("********************Failed to connect, reason: " + ex.Message + "********************************");
                return BadRequest(ex.Message);
            }
        }

        //Get tables from the source details
        // POST api/<controller>
        [Route("api/DataBase/input")]
        public object Post(DbSourceDetails input)
        {
            _logger.Info("Getting tables from source: " + input.srcIPAddress);
            srcIPAddress = input.srcIPAddress;
            srcUserName = input.srcUserName;
            srcPassword = input.srcPassword;
            srcDbNames.Add(input.srcDvsName);
            srcDbNames.Add(input.srcDbmName);
            srcPort = input.srcPort;
            string currDB = "firstDB";
            foreach (string srcDbName in srcDbNames)
            {
                if (srcDbName != null && srcDbName != "")
                {
                    var connectionString = "server=" + srcIPAddress + "; port=" + srcPort + ";username=" + srcUserName + "; password=" + srcPassword + ";database=" + srcDbName;
                    string query = "show full tables from " + srcDbName + " where Table_Type = 'BASE TABLE';";
                    string query1 = "show full tables in " + srcDbName + " where table_type like 'view';";
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    {
                        MySqlCommand command = new MySqlCommand(query, connection);

                        // Open the connection in a try/catch block. 
                        // Create and execute the DataReader, writing the result
                        // set to the console window.
                        try
                        {
                            connection.Open();
                            _logger.Info("connection is opened");
                            DataTable dataTable = new DataTable();
                            _logger.Info("Executing query to get available tables from: " + srcDbName);
                            MySqlDataReader reader = command.ExecuteReader();
                            while (reader.Read())
                            {
                                if (currDB == "firstDB")
                                {
                                    tableNames1.Add(reader.GetString(0));
                                }
                                else if (currDB == "secondDB")
                                {
                                    tableNames2.Add(reader.GetString(0));
                                }
                            }
                            dataTable.Load(reader);
                            reader.Close();
                            _logger.Info("Read tables from source DB: " + srcDbName + "; completed.");
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Error is " + ex.Message);
                            message = string.Format("Message: {0}", ex.InnerException.Message);
                            return new { tableNames1, viewNames1, tableNames2, viewNames2, message };
                        }

                        MySqlCommand command1 = new MySqlCommand(query1, connection);

                        // Open the connection in a try/catch block. 
                        // Create and execute the DataReader, writing the result
                        // set to the console window.
                        try
                        {

                            DataTable dataTable = new DataTable();
                            _logger.Info("Executing query to get view available in " + srcDbName);
                            MySqlDataReader reader = command1.ExecuteReader();
                            while (reader.Read())
                            {
                                if (currDB == "firstDB")
                                {
                                    viewNames1.Add(reader.GetString(0));
                                }
                                else if (currDB == "secondDB")
                                {
                                    viewNames2.Add(reader.GetString(0));
                                }
                            }
                            dataTable.Load(reader);
                            reader.Close();
                            connection.Close();
                            _logger.Info("Read Views from source DB: " + srcDbName + "; completed.");
                            _logger.Info("connection is closed");
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Error is " + ex.Message);
                            message = string.Format("Message: {0}\\n\\n", ex.InnerException);
                        }
                    }
                }
                currDB = "secondDB";
            }
            return new { tableNames1, viewNames1, tableNames2, viewNames2, message };
        }

        //checking if selected tables and views already exists in the destination databases (also deleting if model.deleteTablesNviews == true)
        // POST api/<controller>
        [Route("api/DataBase/check")]
        public object PostCheck(DbDestinationDetails model)
        {
            List<string> msg = new List<string>();
            desIPAddress = model.desIPAddress;
            desUserName = model.desUsername;
            desPassword = model.desPassword;
            desPort = model.desPort;
            srcIPAddress = model.sourceDetails.srcIPAddress;
            srcUserName = model.sourceDetails.srcUserName;
            srcPassword = model.sourceDetails.srcPassword;
            srcPort = model.sourceDetails.srcPort;
            sourceDBNames.Add(model.sourceDetails.srcDvsName);
            sourceDBNames.Add(model.sourceDetails.srcDbmName);
            desDBNames.Add(model.desDvsName);
            desDBNames.Add(model.desDbmName);

            var connectionString = "server=" + desIPAddress + "; port=" + desPort + ";username=" + desUserName + "; password=" + desPassword;

            string[] tables1 = model.selectedTable.Split(';');
            string[] views1 = model.selectedView.Split(';');
            string[] tables2 = model.selectedTable2.Split(';');
            string[] views2 = model.selectedView2.Split(';');
            if (model.deleteTablesNviews == false)
            {
                _logger.Info("Checking if selected tables and views already exists in destination Database");
            }
            else if(model.deleteTablesNviews == true)
            {
                _logger.Info("Deleting selected tables and views which already exists in destination Database");
            }

            for (int i = 1; i >= 0; i--)
            {
                string sourceDBName = sourceDBNames[i];
                string desDBName = desDBNames[i];
                string[] tables;
                string[] views;
                if (i == 1)
                {
                    tables = tables2;
                    views = views2;
                }
                else
                {
                    tables = tables1;
                    views = views1;
                }

                foreach (string table in tables)
                {
                    if (table != "")
                    {
                        string checkTableQuery = string.Format("SELECT EXISTS( SELECT `TABLE_NAME` FROM `INFORMATION_SCHEMA`.`TABLES` WHERE (`TABLE_NAME` = '{0}') AND (`TABLE_SCHEMA` = '{1}')) as `is-exists`;", table, desDBName);
                        var count = Convert.ToInt32(DataBaseHelper.GetFirstFieldValueFromQuery(connectionString, checkTableQuery));
                        if(count > 0)
                        {
                            if (model.deleteTablesNviews == true)
                            {
                                string dropTableQuery = string.Format("Drop Table `{0}`.`{1}`;", desDBName, table);
                                try
                                {
                                    DataTable dt = DataBaseHelper.ExecuteQuery(connectionString, dropTableQuery);
                                }
                                catch(Exception ex) {
                                    _logger.Error(ex.Message);
                                }
                            }
                            else
                            {
                                string outputMsg = string.Format("Table '{0}' already exists in Database '{1}'", table, desDBName);
                                _logger.Info(outputMsg);
                                msg.Add(outputMsg);
                            }
                        }
                    }
                }
                foreach ( string view in views )
                {
                    if (view != "")
                    {
                        string checkViewQuery = string.Format("SELECT EXISTS( SELECT `TABLE_NAME` FROM `INFORMATION_SCHEMA`.`VIEWS` WHERE (`TABLE_NAME` = '{0}') AND (`TABLE_SCHEMA` = '{1}')) as `is-exists`;", view, desDBName);
                        var count = Convert.ToInt32(DataBaseHelper.GetFirstFieldValueFromQuery(connectionString, checkViewQuery));
                        if (count > 0)
                        {
                            if (model.deleteTablesNviews == true)
                            {
                                string dropViewQuery = string.Format("Drop View `{0}`.`{1}`;", desDBName, view);
                                try
                                {
                                    DataTable dt = DataBaseHelper.ExecuteQuery(connectionString, dropViewQuery);
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(ex.Message);
                                }
                            }
                            else
                            {
                                string outputMsg = string.Format("View '{0}' already exists in Database '{1}'", view, desDBName);
                                _logger.Info(outputMsg);
                                msg.Add(outputMsg);
                            }
                        }
                    }
                }

            }

            if (model.deleteTablesNviews == false)
            {
                _logger.Info("*****************Checking if selected tables and views already exists in destination Database completed******************");
            }
            else if (model.deleteTablesNviews == true)
            {
                _logger.Info("*****************Deleting selected tables and views which already exists in destination Database completed*****************");
            }

            return msg;
        }

        //copying tables and views
        // POST api/<controller>
        [Route("api/DataBase/model")]
        public object Post(DbDestinationDetails model)
        {
            message = string.Empty;
            desIPAddress = model.desIPAddress;
            desUserName = model.desUsername;
            desPassword = model.desPassword;
            desDBNames.Add(model.desDvsName);
            desDBNames.Add(model.desDbmName);
            desPort = model.desPort;
            srcIPAddress = model.sourceDetails.srcIPAddress;
            srcUserName = model.sourceDetails.srcUserName;
            srcPassword = model.sourceDetails.srcPassword;
            srcPort = model.sourceDetails.srcPort;
            sourceDBNames.Add(model.sourceDetails.srcDvsName);
            sourceDBNames.Add(model.sourceDetails.srcDbmName);

            var connectionString = "server=" + desIPAddress + "; port=" + desPort + ";username=" + desUserName + "; password=" + desPassword;

            string[] tables1 = model.selectedTable.Split(';');
            string[] views1 = model.selectedView.Split(';');
            string[] tables2 = model.selectedTable2.Split(';');
            string[] views2 = model.selectedView2.Split(';');

            for (int i = 1; i >= 0; i--)
            {
                string sourceDBName = sourceDBNames[i];
                string desDBName = desDBNames[i];
                if (sourceDBName != null && desDBName != null)
                {
                    string[] tables;
                    string[] views;
                    if (i == 1)
                    {
                        tables = tables2;
                        views = views2;
                    }
                    else
                    {
                        tables = tables1;
                        views = views1;
                    }


                    //creating schema
                    string MySqlCreateSchema = string.Format("CREATE SCHEMA `{0}`", desDBName);
                    _logger.Info("Creating schema: " + desDBName + " if doesn't exists, in destination.");
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    {
                        MySqlCommand command = new MySqlCommand(MySqlCreateSchema, connection);

                        // Open the connection in a try/catch block. 
                        // Create and execute the DataReader, writing the result
                        // set to the console window.
                        try
                        {
                            connection.Open();
                            _logger.Info("connection is opened");
                            DataTable dataTable = new DataTable();
                            MySqlDataReader reader = command.ExecuteReader();
                            dataTable.Load(reader);
                            _logger.Info("Creating Schema done");
                            reader.Close();
                            connection.Close();
                            _logger.Info("connection is closed");
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("create schema " + desDBName + " failed. " + ex.Message + " \n skipping create schema.");
                            //message += string.Format("Message: {0} ", ex.Message);
                            //return message;
                        }
                    }

                    connectionString = "server=" + desIPAddress + "; port=" + desPort + ";username=" + desUserName + "; password=" + desPassword + ";database=" + desDBName;

                    foreach (string table in tables)
                    {
                        _logger.Info("Copying table: " + table + " from source path");
                        if (table != "")
                        {
                            DataTable dataTableS = new DataTable();
                            //getting table from source
                            string sourceConnString = "server=" + srcIPAddress + "; port=" + srcPort + ";username=" + srcUserName + "; password=" + srcPassword + ";database=" + sourceDBName;
                            string mySqlSource = "show create table " + sourceDBName + "." + table;
                            using (MySqlConnection connectionS = new MySqlConnection(sourceConnString))
                            {
                                MySqlCommand commandS = new MySqlCommand(mySqlSource, connectionS);

                                // Open the connection in a try/catch block. 
                                // Create and execute the DataReader, writing the result
                                // set to the console window.
                                try
                                {
                                    connectionS.Open();

                                    MySqlDataReader reader = commandS.ExecuteReader();
                                    dataTableS.Load(reader);
                                    reader.Close();
                                    connectionS.Close();
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error("Reading table " + table + " failed. " + ex.Message);
                                    message += string.Format("Message: {0} ", ex.Message);
                                }
                            }
                            string tableQuery = Convert.ToString(DataBaseHelper.GetColumnValue("Create Table", dataTableS));
                            tableQuery = tableQuery.Replace("\n", "");
                            tableQuery = tableQuery.Replace("TABLE `", "TABLE `" + desDBName + "`.`");

                            //copying to destination
                            string MySql = tableQuery;

                            using (MySqlConnection connection = new MySqlConnection(connectionString))
                            {
                                MySqlCommand command = new MySqlCommand(MySql, connection);

                                // Open the connection in a try/catch block. 
                                // Create and execute the DataReader, writing the result
                                // set to the console window.
                                try
                                {
                                    connection.Open();
                                    _logger.Info("connection is opened");
                                    DataTable dataTable = new DataTable();
                                    MySqlDataReader reader = command.ExecuteReader();
                                    dataTable.Load(reader);
                                    _logger.Info("Copying table: " + table + " done");
                                    reader.Close();
                                    connection.Close();
                                    _logger.Info("connection is closed");

                                    //copying table data
                                    _logger.Info("copying table data for table: " + table);
                                    string destConnString = "server=" + desIPAddress + "; port=" + desPort + ";username=" + desUserName + "; password=" + desPassword + ";database=" + desDBName;
                                    MySqlConnection srcConnection = new MySqlConnection(sourceConnString);
                                    MySqlConnection desConnection = new MySqlConnection(destConnString);
                                    CopyTable.CopyTableData(srcConnection, desConnection, "select * from " + table, table);
                                    _logger.Info("table data copied successfully.");
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error("copying table " + table + " failed. " + ex.Message);
                                    message += string.Format("Message: {0} ", ex.Message);
                                }
                            }
                        }
                    }

                    _logger.Info("*******************************Copyiny tables completed**************************");

                    foreach (string view in views)
                    {
                        _logger.Info("Copying view: " + view + " from source path");
                        if (view != "")
                        {
                            DataTable dataViewS = new DataTable();
                            //getting table from source
                            string sourceConnString = "server=" + srcIPAddress + "; port=" + srcPort + ";username=" + srcUserName + "; password=" + srcPassword + ";database=" + sourceDBName;
                            string mySqlSource = "show create view " + sourceDBName + "." + view;
                            using (MySqlConnection connectionS = new MySqlConnection(sourceConnString))
                            {
                                MySqlCommand commandS = new MySqlCommand(mySqlSource, connectionS);

                                // Open the connection in a try/catch block. 
                                // Create and execute the DataReader, writing the result
                                // set to the console window.
                                try
                                {
                                    connectionS.Open();

                                    MySqlDataReader reader = commandS.ExecuteReader();
                                    dataViewS.Load(reader);
                                    reader.Close();
                                    connectionS.Close();
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error("Reading view " + view + " failed. " + ex.Message);
                                    message += string.Format("Message: {0} ", ex.Message);
                                }
                            }
                            string viewQuery = Convert.ToString(DataBaseHelper.GetColumnValue("Create View", dataViewS));

                            string MySql1 = viewQuery;
                            if (i == 0)
                            {
                                MySql1 = MySql1.Replace("VIEW `" + view, "VIEW `" + desDBName + "`.`" + view);
                                MySql1 = MySql1.Replace(sourceDBNames[1], desDBNames[1].Trim());
                            }

                            using (MySqlConnection connection = new MySqlConnection(connectionString))
                            {

                                MySqlCommand command1 = new MySqlCommand(MySql1, connection);

                                // Open the connection in a try/catch block. 
                                // Create and execute the DataReader, writing the result
                                // set to the console window.
                                try
                                {
                                    connection.Open();
                                    _logger.Info("connection is opened");
                                    DataTable dataTable = new DataTable();
                                    MySqlDataReader reader = command1.ExecuteReader();
                                    dataTable.Load(reader);
                                    _logger.Info("Copying view: " + view + " done");
                                    reader.Close();
                                    connection.Close();
                                    _logger.Info("connection is closed");
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error("Copying view " + view + " failed. " + ex.Message);
                                    //message += string.Format("Message: {0} ", ex.Message);
                                }
                            }
                        }
                    }

                    _logger.Info("*******************************Copyiny views completed**************************");

                }
            }
            if(message.Length < 1)
            {
                message = "Copied Successfully";
            }
            else
            {
                message += "Message: Copied Successfully";
            }
            _logger.Info("***********************************copying tables and views completed**********************************************");
            return message;

        }

   // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
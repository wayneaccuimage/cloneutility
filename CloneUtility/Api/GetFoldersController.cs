﻿using CloneUtility.Models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace CloneUtility
{
    public class GetFoldersController : ApiController
    {
        private static ILog _logger = LogManager.GetLogger(typeof(GetFoldersController));
        string sServerName;
        string sPath;
        string sServerPath;
        string sUserName;
        string sPassword;
        string ini;
        string dServerName;
        string dTargetPath;
        string dTargetFolder;
        string srcServer;
        string srcPath;
        string srcUser;
        string srcPwd;
        string dUserName;
        string dPassword;
        string message;
        string filename;
        List<object> values = new List<object>();
        string ipAddress = string.Empty;
        List<object> files = new List<object>();
        List<object> folders = new List<object>();
        List<object> filesInFolder = new List<object>();
        string coreDatabase = string.Empty;
        string sectionName = string.Empty;
        string keyName = string.Empty;
        string value = string.Empty;


        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [Route("api/GetFolders/data")]

        public object Post(SourceDetails data)
        {
            _logger.Info("Getting details of C$\\Program Files (x86)\\AccuImage");
            sServerName = data.sServerName;
            sPath = data.sPath;
            sPath = sPath.Replace("/", "\\");
            sUserName = data.sUserName;
            sPassword = data.sPassword;
            sServerPath = Path.Combine(sServerName, sPath);
            var path = @"  " + sServerPath + " ";
            var sourcPath = Path.Combine(path, data.selectedFolder);

            _logger.Info("Connecting to source server");
            var impersonationContext = new WrappedImpersonationContext(sServerPath, sUserName, sPassword);
            impersonationContext.Enter();
            //do your stuff here

            sServerName = sServerName.Replace(@"\", string.Empty);
            ipAddress = Dns.GetHostAddresses(sServerName).ToList().Where(a => ( a.ToString().IndexOf(".")) > 0 ).FirstOrDefault().ToString();
            _logger.Info("Connected");
                try
                {
                   _logger.Info("Getting folders from" +sPath);
                    DirectoryInfo share = new DirectoryInfo(path);
                    foreach (DirectoryInfo child in share.GetDirectories())
                    {
                        folders.Add(child.ToString());
                    _logger.Info("Getting Folders :" +child.ToString());
                }
                    _logger.Info("Getting Folders Done");
                }
                catch (Exception e)
                {
                    _logger.Error("Error is " + e.Message);
                    message = string.Format("Message: {0}\\n\\n", e.Message);
                }

            if (data.showFiles == 1)            //if true get files available files from selected folder
            {
                try
                {
                    _logger.Info("Getting files from" +sPath);
                    foreach (string f in Directory.GetFileSystemEntries(sourcPath))
                    {
                        string tempFileName = Path.GetFileName(f);
                        filesInFolder.Add(tempFileName);
                        _logger.Info("Getting file:" +tempFileName.ToString());
                    }
                    _logger.Info("Getting files done");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                }
                data.showFiles = 0;
            }


            if (data.ini == "1")  //if true read the selected ini file
            {
                try
                {
                    _logger.Info("Reading ini file");
                    if (data.selectedFile.Substring(data.selectedFile.Length - 3) == "ini")
                    {
                        string filename = sourcPath + "\\" + data.selectedFile;
                        string file = filename.ToString();
                        FileStream fr = new FileStream(filename, FileMode.Open, FileAccess.Read);
                        StreamReader sr = new StreamReader(fr);
                        string r = sr.ReadToEnd();
                        string[] keys  = Regex.Split(r, "\r\n");
                        foreach (var key in keys)
                        {
                            values.Add(key.ToString());
                            _logger.Info("Reading ini:" +key.ToString());
                        }
                        fr.Close();
                        sr.Close();
                        _logger.Info("Reading ini file done");
                    }
                }
                catch (Exception e)
                {
                    _logger.Error("Error is " + e.Message);
                    message = string.Format("Message: {0}\\n\\n", e.Message);
                }
            }
            impersonationContext.Leave();
            return new {ipAddress, values, folders, filesInFolder, message };
        }
           
        //copying files to destination - targetpath
        [Route("api/GetFolders/value")]
        public object Post(DestinationDetails value)
        {
            _logger.Info("Getting details of Destination");
            dTargetFolder = value.dTargetFolder;
            dTargetPath = value.dTargetPath;
            dTargetPath = dTargetPath.Replace("/", "\\");
            dServerName = value.dServerName;
            dUserName = value.dUserName;
            dPassword = value.dPassword;
            srcServer = value.sServerName;
            srcPath = value.sPath;
            srcPath = srcPath.Replace("/", "\\");
            srcUser = value.sUserName;
            srcPwd = value.sPassword;
            var sourcePath = Path.Combine(srcServer, srcPath);
            var sourcPath = Path.Combine(sourcePath, value.selectedFolder);
            var dPath = Path.Combine(dTargetPath, dTargetFolder);
            var path = Path.Combine(dServerName, dPath);

            _logger.Info("Connecting to destination server");
            var impersonationContext = new WrappedImpersonationContext(dServerName, dUserName, dPassword);
            impersonationContext.Enter();

            _logger.Info("Connected");

            dServerName = dServerName.Replace(@"\", string.Empty);

            try
            {
                ipAddress = Dns.GetHostAddresses(dServerName).ToList().Where(a => (a.ToString().IndexOf(".")) > 0).FirstOrDefault().ToString();
                _logger.Info("Connecting to source server for getting files");
                NetworkCredential NCredentials = new NetworkCredential(srcUser, srcPwd);
                using (new NetworkConnection(sourcPath, NCredentials))
                {
                    _logger.Info("Connected");
                    _logger.Info("Checking if path exist or not");
                    if (!Directory.Exists(path))
                    {
                        _logger.Info("Path doesn't exist in destination");
                        Directory.CreateDirectory(path);
                        _logger.Info("Creates Path");
                        foreach (string sourceSubFolder in Directory.GetDirectories(sourcPath, "*", SearchOption.AllDirectories).Where(directory => !directory.Contains("Download") || !directory.Contains("download")))
                        {
                            if (!(sourceSubFolder.IndexOf("download") > -1))
                            {
                                Directory.CreateDirectory(sourceSubFolder.Replace(sourcPath, path));
                            }
                        }
                        // Copy files
                        foreach (string sourceFile in Directory.GetFiles(sourcPath, "*", SearchOption.AllDirectories).Where(directory => !directory.Contains("Download") || !directory.Contains("download")))
                        {
                            if (!(sourceFile.IndexOf("download") > -1))
                            {
                                string destinationFile = sourceFile.Replace(sourcPath, path);
                                int Index = destinationFile.LastIndexOf("\\");
                                string destination = destinationFile.Substring(Index + 1);
                                File.Copy(sourceFile, destinationFile, true);
                                _logger.Info("Copying files from source to destination");
                                files.Add(destination.ToString());
                                _logger.Info("Files copied:" + destination.ToString());
                            }
                        }
                        _logger.Info("Copying Files Done");
                        var access = GrantAccess(path);
                        string[] filePath = Directory.GetFiles(path, "*DVStream.ini");
                        foreach (var filename in filePath)
                        {
                            string file = filename.ToString();

                            INIFile ini = new INIFile(filename);
                            _logger.Info("Reading " + filename);
                            coreDatabase = ini.IniReadValue("DVSTREAM", "CoreDatabase");
                        }
                    }
                    if (Directory.Exists(path))
                    {
                        _logger.Info(dPath + " already exists");
                        throw new Exception(dTargetFolder + " already Exists");
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error("Error is " + e.Message);
                message = string.Format("Message: {0}", e.Message);
                if(message.IndexOf("remote share") > 0)
                {
                    message = message.Replace("remote share", "remote share : ");
                }
            }
            _logger.Info("*********************copy files completed*************************");
            return new { ipAddress, coreDatabase, files, message };

        }
        //grant security permissions to copied folder
        private bool GrantAccess(string fullPath)
        {
            DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
            return true;
        }

        //updating ini file
        // POST api/<controller>
        [Route("api/GetFolders/load")]
        public object Post(IniDetails load)
        {

            _logger.Info("Getting Details");
            sectionName = load.sectionName;
            keyName = load.keyName;
            value = load.value;
            dTargetPath = load.dTargetPath;
            dTargetPath = dTargetPath.Replace("/", "\\");
            dServerName = load.dServerName;
            dUserName = load.dUserName;
            dPassword = load.dPassword;
            var dPath = Path.Combine(dTargetPath, load.selectedFolder);
            var path = Path.Combine(dServerName, dPath);

            _logger.Info("Connecting to " + path);
            var impersonationContext = new WrappedImpersonationContext(dServerName, dUserName, dPassword);
            impersonationContext.Enter();
            _logger.Info("Connected");
            try
            {
                _logger.Info("Getting INI file from " + path);
                string[] filePath = Directory.GetFiles(path, "*.ini");
                foreach (var filename in filePath)
                {
                    string file = filename.ToString();
                    INIFile ini = new INIFile(filename);
                    _logger.Info("writing" + filename);
                    ini.IniWriteValue(sectionName, keyName, value);
                    _logger.Info("updated Ini");
                }
            }
            catch (Exception e)
            {
                _logger.Error("Error is " + e.Message);
                message = string.Format("Message: {0}", e.Message);
            }
            return new { values, message };

        }


        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
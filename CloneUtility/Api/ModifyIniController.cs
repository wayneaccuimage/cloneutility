﻿using CloneUtility.Models;
using log4net;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace CloneUtility.Api
{
    public class ModifyIniController : ApiController
    {
        private static ILog _logger = LogManager.GetLogger(typeof(ModifyIniController));
        string message;
        string dServerName;
        string dTargetPath;
        string dTargetFolder;
        string dUserName;
        string dPassword;
        string myTextbox;

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        //saving the modified ini file from the textarea
        // POST api/<controller>
        [Route("api/modifyIni/value")]
        public object Post(DestinationDetails value)
        {

            _logger.Info("Getting Details of destination C$\\Program Files (x86)\\AccuImage");
            myTextbox = value.myTextbox;
            dTargetFolder = value.dTargetFolder;
            dTargetPath = value.dTargetPath;
            dTargetPath = dTargetPath.Replace("/", "\\");
            dServerName = value.dServerName;
            dUserName = value.dUserName;
            dPassword = value.dPassword;
            var dPath = Path.Combine(dTargetPath, dTargetFolder);
            var path = Path.Combine(dServerName, dPath);

            _logger.Info("Connecting to " + path);
            var impersonationContext = new WrappedImpersonationContext(dServerName, dUserName, dPassword);
            impersonationContext.Enter();
            _logger.Info("Connected");

            try
            {
                _logger.Info("Getting ini file from " + path);
                var fileName = Path.Combine(path, value.selectedFile);
                string file = fileName.ToString();
                string[] values = Regex.Split(myTextbox, "\n");
                File.Delete(file);
                File.WriteAllLines(file, values);              
                _logger.Info("Replacing ini file from " + path);
            }
            catch (Exception e)
            {
                _logger.Error("Error is " + e.Message);
                message = string.Format("Message: {0}", e.Message);
            }
            return new { message };

        }

       // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
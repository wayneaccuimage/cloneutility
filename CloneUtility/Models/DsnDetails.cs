﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloneUtility.Models
{
    public class DsnDetails
    {
        public string dsnName { get; set; }
        public string driverName { get; set; }
        public string dsnType { get; set; }
        public string serverName { get; set; }
        public string iPAddress { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string port { get; set; }
        public string dataBase { get; set; }
    }
}
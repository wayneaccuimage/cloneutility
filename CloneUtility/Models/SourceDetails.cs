﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloneUtility.Models
{
    public class SourceDetails
    {
        public string sServerName { get; set; }
        public string sPath { get; set; }
        public string sUserName { get; set; }
        public string sPassword { get; set; }
        public string selectedFolder { get; set; }
        public string selectedFile { get; set; }
        public int showFiles { get; set; }
        public string ini { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloneUtility.Models
{
    public class DestinationDetails
    {
        public string dServerName { get; set; }
        public string dTargetPath { get; set; }
        public string dTargetFolder { get; set; }
        public string selectedFolder { get; set; }
        public string sServerName { get; set; }
        public string sPath { get; set; }
        public string sUserName { get; set; }
        public string sPassword { get; set; }
        public string dUserName { get; set; }
        public string dPassword { get; set; }
        public string selectedFile { get; set; }
        public string myTextbox { get; set; }

    }
}
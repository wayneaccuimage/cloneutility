﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloneUtility.Models
{
    public class DbSourceDetails
    {
        public string srcIPAddress { get; set; }
        public string srcUserName { get; set; }
        public string srcPassword { get; set; }
        public string srcDvsName { get; set; }
        public string srcDbmName { get; set; }
        public string srcPort { get; set; }
    }
}
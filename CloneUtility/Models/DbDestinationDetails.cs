﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloneUtility.Models
{
    public class DbDestinationDetails
    {
        public string desIPAddress { get; set; }
        public string desUsername { get; set; }
        public string desPassword { get; set; }
        public string desDvsName { get; set; }
        public string desDbmName { get; set; }
        public string desPort { get; set; }
        public string desFolderPath { get; set; }
        public string selectedTable { get; set; }
        public string selectedView { get; set; }
        public string selectedTable2 { get; set; }
        public string selectedView2 { get; set; }
        //public string srcDBName { get; set; }
        public string desTableName { get; set; }
        public DbSourceDetails sourceDetails { get; set; }
        public List<object> sysSettings { get; set; }
        public bool deleteTablesNviews { get; set; }

    }
}
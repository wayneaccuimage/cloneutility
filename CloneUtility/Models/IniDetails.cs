﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloneUtility.Models
{
    public class IniDetails
    {
        public string sectionName { get; set; }
        public string keyName { get; set; }
        public string value { get; set; }
        public string dServerName { get; set; }
        public string selectedFolder { get; set; }
        public string dUserName { get; set; }
        public string dPassword { get; set; }
        public string dTargetPath { get; set; }
    }
}

﻿var cloneUtilityApp = angular.module("cloneUtilityApp", ['ngRoute']);

cloneUtilityApp.factory('webServices',['$http', function ($http) {
    var data = {};

    data.getItems = function (serviceUrl, queryString) {
        return $http({
            method: 'GET',
            url: serviceUrl + queryString
        });
    };

    data.deleteItem = function (serviceUrl) {
        return $http.delete(serviceUrl);
    };

    data.putItem = function (serviceUrl, item) {
        return $http.put(serviceUrl, item);
    };

    data.postItem = function (serviceUrl, item) {
        return $http.post(serviceUrl, item);
    };

    return data;
}]);


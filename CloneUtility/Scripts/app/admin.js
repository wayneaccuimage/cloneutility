﻿var getFoldersUrl = "api/GetFolders/data";
var copyFoldersUrl = "api/GetFolders/value";
var getTablesUrl = "api/DataBase/input";
var createTablesUrl = "api/DataBase/model";
var checkTableUrl = "api/DataBase/check";
var verifyConnectionUrl = "api/DataBase/verify";
var createDsnUrl = "api/Dsn";
var modifyUrl = "api/ViewIni/value";
var getSystemSettingsUrl = "api/SystemSettings/model";
var systemSettingsUrl = "api/SystemSettings";
var addKeyToEntityUrl = "api/SystemSettings/addSett";
var editINIUrl = "api/GetFolders/load";
var replaceINIUrl = "api/modifyIni/value";

cloneUtilityApp.controller('mainCtrl', ['$scope', 'webServices', '$rootScope', function ($scope, webServices, toaster, $rootScope) {
    //clear toastr
    $(document).keypress(function (e) {
        toastr.clear();
    });

//SERVER TAB
    $scope.selectedFolder = "";
    $scope.input = {};
    $scope.model = {};
    $scope.value = {};
    $scope.display = false;
    $scope.displayButton = false;
    $scope.result = false;
    $scope.resultFiles = false;
    $scope.server = false;
    $scope.checkall = false;
    $scope.checkedall = false;

    // use source
    $scope.usesource = function (item) {
        $scope.value.dServerName = item.sServerName;
        $scope.value.dUserName = item.sUserName;
        $scope.value.dPassword = item.sPassword;
    }

    //get source folders, files in folder and read an ini file
    $scope.getService = function (data) {
        $scope.display = true;
        $scope.data.dataLoading = true;
        $scope.folders = {};
        data.selectedFolder = $scope.selectedFolder;
        webServices.postItem(getFoldersUrl, data).success(function (response) {
            $scope.results = response;
            $scope.folders = response.folders;
            $scope.values = response.values;
            $scope.filesInFolder = response.filesInFolder;
            $scope.input.srcIPAddress = response.ipAddress;
            $scope.displayButton = true;
            $scope.result = true;
           if ($scope.folders.length==0) {
               alert(response.message);
               toastr.error("Couldn't load folders");
                 $scope.result = false;
                $scope.displayButton = false;
            }
            if ($scope.filesInFolder.length == 0) {
                $scope.resultFiles = false;
            }
            $scope.data.dataLoading = false;
            $scope.display = false;
            if ($scope.values.length > 1) {
                $scope.body = "<body><div>";
                for (i = 0; i < $scope.values.length; i++) {
                    if ($scope.values[i].length > 0) {
                        $scope.body += "<p>" + $scope.values[i] + "</p>";
                    }
                }
                $scope.body += "</div></body>";

                var newWindow = window.open("", "", "width=500, height=600, scrollbars=yes");
                newWindow.document.write($scope.body);
            }
        }).error(function (error) {
            toastr.error("Couldn't load files");
            $scope.display = false;
            $scope.data.dataLoading = false;
        });
    }

    //To get folders from source(Get Folders btn)
    $scope.getFolders = function (data) {
        $scope.server = false;
        data.showFiles = 0;
        data.ini = "0";
        $scope.getService(data);        
    }

    //To show files in folder (On double click on any folder)
    $scope.showFilesInFolder = function (data) {
        $scope.resultFiles = true;
        $scope.server = true;
        $scope.selected = $scope.selectedFolder;
        data.showFiles = 1;
        data.ini = "0";
        $scope.getService(data);
    }

    //To Read ini file (On double click on any ini file)
    $scope.displayFiles = function (data) {
            data.ini = "1";
            $scope.getService(data);
    }

    $scope.progress = false;
    $scope.updateButton = false;
    $scope.modifyButton = false;
    $scope.modifyPanel = false;
    $scope.showCopyApplication = true;

    //copy selected server to destination (copy Application btn function)
    $scope.copyItem = function (value, sServerName, sPath, sUserName, sPassword) {
        if (value.dServerName.length > 0 && value.dUserName.length > 0 && value.dPassword.length > 0 && value.dTargetFolder.length > 0) {
            $scope.showCopyApplication = false;
            $scope.progressbar = false;
            $scope.progress = true;
            $scope.dataLoading = true;
            value.selectedFolder = $scope.selectedFolder;
            value.sServerName = sServerName;
            value.sPath = sPath;
            value.sUserName = sUserName;
            value.sPassword = sPassword;
            $scope.files = {};
            $scope.coreDatabase = [];
            var confirmation = confirm("CopyStream \n Source server : " + value.sServerName + "\n Directory : " + value.selectedFolder + " \n \n Destination Server: " + value.dServerName + "\n Destination directory: " + value.dTargetFolder);
            //if confirmation variable contain true; continue to copy server.
            if (confirmation) {
                if (value.selectedFolder != "" && value.selectedFolder != null) {
                    webServices.postItem(copyFoldersUrl, value).success(function (response) {
                        $scope.results = response;
                        $scope.files = response.files;
                        $scope.model.desIPAddress = response.ipAddress;
                        //storing the coreDatabase value from the ini file
                        $scope.coreDatabase = response.coreDatabase.split(";");
                        for (var c = 0; c < $scope.coreDatabase.length; c++) {
                            var temp = $scope.coreDatabase[c].indexOf("DATABASE");
                            if (temp >= 0) {
                                $scope.input.srcDvsName = $scope.coreDatabase[c].substring($scope.coreDatabase[c].indexOf("=") + 1);
                            }
                        }
                        $scope.progressbar = true;
                        $scope.updateButton = true;
                        $scope.modifyButton = true;
                        $scope.modifyPanel = true;
                        $scope.showCopyApplication = true;
                        if ($scope.files.length == 0) {
                            alert(response.message);
                            toastr.error("Couldn't load files");
                            $scope.progressbar = false;
                            $scope.updateButton = false;
                            $scope.modifyButton = false;
                            $scope.modifyPanel = false;
                        } else {
                            toastr.success("Successfully copied to destination folder");
                        }
                        $scope.dataLoading = false;
                        $scope.progress = false;
                    }).error(function (er) {
                        toastr.error(er);
                        $scope.showCopyApplication = true;
                    });
                }
                else {
                    toastr.error("No Folder Selected");
                    $scope.dataLoading = false;
                    $scope.progress = false;
                    $scope.showCopyApplication = true;
                }
            }
            else {
                toastr.info("Copying Aborted");
                $scope.dataLoading = false;
                $scope.progress = false;
                $scope.showCopyApplication = true;
            }
        }
        else {
            toastr.info("Please fill all fields");
        }
    }

    //edit ini file's individual values (used in this code to update ROOTPATH and coreDatabase)
    $scope.modifyBar = false;
    $scope.editItem = function (load, destDetails) {
        $scope.modifyLoading = true;
        load.selectedFolder = $scope.value.dTargetFolder;
        load.dServerName = destDetails.dServerName;
        load.dUserName = destDetails.dUserName;
        load.dPassword = destDetails.dPassword;
        load.dTargetPath = destDetails.dTargetPath;
        webServices.postItem(editINIUrl, load).success(function (response) {
            $scope.results = response;
            $scope.IniMessage = "Updated Successfully"
            //toastr.success("Successfully loaded");
            $scope.modifyBar = true;
            $scope.IniMsg = true;
        }).error(function (error) {
            toastr.error("Couldn't save ini");
        });
    }
    
//save modified ini values from the textarea
    $scope.replaceItem = function (value) {
        $scope.modifyLoading = true;
        value.myTextbox = $scope.contents;
        value.selectedFolder = $scope.selectedFolder;
        //value.sServerName = sServerName;
        //value.sPath = sPath;
        //value.sUserName = sUserName;
        //value.sPassword = sPassword;
        value.selectedFile = $scope.copiedFolder;
        webServices.postItem(replaceINIUrl, value).success(function (response) {
            $scope.results = response;
            $scope.updateIniMessage = "Updated Successfully"
            toastr.success("Updated Successfully");
            $scope.modifyBar = true;
            $scope.IniMsg = true;
        }).error(function (error) {
            toastr.error("failed to update");
        });
    }

//TO load the ini file in new window and also into textarea (from destination)
    $scope.contents = "";
    $scope.modifyItem = function (value, fileName) {
        $scope.contents = "";
        value.selectedFolder = $scope.selectedFolder;
        //value.sServerName = sServerName;
        //value.sPath = sPath;
        //value.sUserName = sUserName;
        //value.sPassword = sPassword;
        value.selectedFile = fileName;
        webServices.postItem(modifyUrl, value).success(function (response) {
            $scope.results = response;
            $scope.points = response.points;
            for (i = 0; i < $scope.points.length; i++) {
                $scope.contents += $scope.points[i] + "\n";
            }
            $scope.modifyBar = true;
            if ($scope.points.length > 1) {
                //display ini file in new window
                //$scope.body = "<body><div>";
                //for (i = 0; i < $scope.points.length; i++) {
                //    if ($scope.points[i].length > 0) {
                //        $scope.body += "<p>" + $scope.points[i] + "</p>";
                //    }
                //}
                //$scope.body += "</div></body>";

                //var newWindow = window.open("", "", "width=500, height=600, scrollbars=yes");
                //newWindow.document.write($scope.body);
                toastr.success("Successfully loaded ini file");
            }

        });

    }
//endOf ini file edit



//Database tab
    $scope.copyDVS = false;
    $scope.copyMaster = false;
    $scope.copyBoth = false;
    $scope.selectedTable = "";
    $scope.selectedView = "";

    $scope.stripes = false;
    $scope.settingsPane = false;

    //to use source IP address for destination
    $scope.usesourceip = function (item) {
        $scope.model.desIPAddress = item.srcIPAddress;
        $scope.model.desPassword = item.srcPassword;
        $scope.model.desPort = item.srcPort;
        $scope.model.desDvsName = "tst_" + item.srcDvsName;
        $scope.model.desDbmName = "tst_" + item.srcDbmName;
    }

    //Verify connection destination
    $scope.verifyConn = function (desDetails) {
        $scope.copyDVS = false;
        $scope.copyMaster = false;
        $scope.copyBoth = false;
        webServices.postItem(verifyConnectionUrl, desDetails).success(function (response) {
            toastr.success("Connection successful");
            if(!$scope.getTablesError){
                if ($scope.input.srcDvsName && $scope.input.srcDvsName != "") {
                    $scope.copyDVS = true;
                    if ($scope.input.srcDbmName && $scope.input.srcDbmName != "") {
                        $scope.copyMaster = true;
                        $scope.copyBoth = true;
                    }
                }
            }
        }).error(function (error) {
            toastr.error("failed to connect");
        });
    }

    //get tables from source
    $scope.getTable = function (input) {
        var continueWithoutMasterDB = true;
        if (input.srcDvsName != "") {
            if (input.srcDbmName == "") {
                continueWithoutMasterDB = confirm("Source Master DB field was empty. \n Click Ok to continue.");
            }
            if (continueWithoutMasterDB) {
                $scope.dvsDB = input.srcDvsName;
                $scope.dbmDB = input.srcDbmName;
                $scope.viewTable = false;
                $scope.table = false;
                $scope.button = false;
                $scope.viewButton = false;
                $scope.stripes = true;
                $scope.barLoading = true;
                $scope.tableNames1 = {};
                $scope.tableNames2 = {};
                $scope.msg = "";
                $scope.detailsForSrcConn = {};
                $scope.detailsForSrcConn.desIPAddress = input.srcIPAddress;
                $scope.detailsForSrcConn.desPort = input.srcPort;
                $scope.detailsForSrcConn.desUsername = input.srcUserName;
                $scope.detailsForSrcConn.desPassword = input.srcPassword;
                //verify connection with source details
                webServices.postItem(verifyConnectionUrl, $scope.detailsForSrcConn).success(function (response) {
                    //if connection successful get tables and views
                    webServices.postItem(getTablesUrl, input).success(function (response) {
                        $scope.getTablesError = false;
                        $scope.results = response;
                        $scope.msg = response.message;
                        if ($scope.msg.length > 1) {
                            toastr.error($scope.msg);
                            $scope.getTablesError = true;
                        } else {
                            toastr.success("Successfully loaded Tables & Views");
                        }
                        $scope.tableMessage = $scope.dvsDB;
                        $scope.tableMessage2 = $scope.dbmDB;
                        $scope.tableNames1 = response.tableNames1;
                        $scope.tableNames2 = response.tableNames2;
                        $scope.table = true;
                        $scope.table2 = true;
                        $scope.button = true;
                        $scope.viewTable = true;
                        $scope.viewTable2 = true;
                        $scope.enable = true;
                        $scope.viewMessage = $scope.dvsDB;
                        $scope.viewMessage2 = $scope.dbmDB;
                        $scope.viewNames1 = response.viewNames1;
                        $scope.viewNames2 = response.viewNames2;
                        $scope.viewButton = true;
                        $scope.Loading = false;
                        $scope.stripes = false;
                        if (response.tableNames1.length == 0) {
                            $scope.table = false;
                            $scope.viewTable = false;
                        }
                        if (response.tableNames2.length == 0) {
                            $scope.table2 = false;
                            $scope.viewTable2 = false;
                        }
                        if (response.viewNames1.length == 0) {
                            $scope.viewMessage = "Views are not available for: " + $scope.dvsDB;
                            $scope.enable = false;
                        }
                        if (response.viewNames2.length == 0) {
                            $scope.viewMessage2 = "Views are not available for: " + $scope.dbmDB;
                            $scope.enable = false;
                        }
                    }).error(function (error) {
                        $scope.stripes = false;
                        $scope.barLoading = false;
                        $scope.getTablesError = true;
                        toastr.error("failed to get tables");
                    });

                }).error(function (error) {
                    $scope.stripes = false;
                    $scope.barLoading = false;
                    toastr.error("failed to connect");
                });
            }
        } else {
            alert("DVStream database field is empty");
        }
    }

      //selectall
    $scope.checkAll = function () {
        if (!$scope.selectedAll) {
            $scope.checkall = true;
        } else {
            $scope.checkall = false;
        }
    }
    //selectALl
    $scope.checkingAll = function () {
        if (!$scope.selectAll) {
            $scope.checkedall = true;
        } else {
            $scope.checkedall = false;
        }
    }

    //to check if tables and views already exists in destination databases
    $scope.checkExists = function (model, sourceInput, destDetails) {
        $scope.tablesExist = [];
        model.sourceDetails = sourceInput;
        model.deleteTablesNviews = false;
        webServices.postItem(checkTableUrl, model).success(function (response) {
            $scope.tablesExist = response;
            if ($scope.tablesExist.length > 0) {
                var msg = "";
                for (var i = 0; i < $scope.tablesExist.length; i++) {
                    msg += $scope.tablesExist[i];
                    msg += "\n";
                }
                msg += "\n\n Click OK to override these tables (or) cancel to abort copying."
                var continueCopy = confirm(msg);
                if (continueCopy) {
                    var promt2 = confirm("This action cannot be undone.\n Are you sure you want to continue?");
                    if (promt2) {
                        //toastr.info("Running process");
                        model.deleteTablesNviews = true;
                        webServices.postItem(checkTableUrl, model).success(function (response) {
                            $scope.tablesExist = response;
                            $scope.create(model, sourceInput, destDetails);
                        });
                    }
                    else {
                        toastr.info("Copy tables Aborted");
                    }
                }
                else {
                    toastr.info("Copy tables Aborted");
                }
            }
            else {
                $scope.create(model, sourceInput, destDetails);
            }
        });
    }

    $scope.copyTableMsg = false;
    $scope.systemSettings = [];
    $scope.proBar = false;
    //copying tables and views to destination database
    $scope.create = function (model, sourceInput, destDetails) {
        $scope.proBar = true;
        $scope.progressLoading = true;
        $scope.viewBar = true;
        $scope.viewLoading = true;
        //model.selectedView = $scope.selectedView;               
        //model.selectedTable = $scope.selectedTable;
        model.sourceDetails = sourceInput;
        webServices.postItem(createTablesUrl, model).success(function (response) {
            $scope.message = response.split('Message:');
            $scope.copiedMessage = $scope.message;
            if ($scope.copiedMessage.length > 0) {
                $scope.copyTableMsg = true;
            }
            toastr.success("successfully copied Tables & Views");

            //updating ini file
            destDetails.dTargetPath = destDetails.dTargetPath.replace("$", ":");

            $scope.iniValues = {};
            $scope.iniValues.sectionName = "DVSTREAM";
            $scope.iniValues.keyName = "RootPath";
            $scope.iniValues.value = destDetails.dTargetPath + "/" + destDetails.dTargetFolder + "/html/";
            //function call to update a key on ini file
            $scope.editItem($scope.iniValues, destDetails);

            $scope.iniValues = {};
            $scope.iniValues.sectionName = "DVSTREAM";
            $scope.iniValues.keyName = "CoreDatabase";
            var iniKeyValue = "";
            for (var c = 0; c < $scope.coreDatabase.length; c++) {
                var temp = $scope.coreDatabase[c].indexOf("DATABASE");
                if (temp >= 0) {
                    $scope.coreDatabase[c] = "DATABASE=" + $scope.model.desDvsName;
                }
                iniKeyValue += $scope.coreDatabase[c];
                if(c < $scope.coreDatabase.length -1){
                    iniKeyValue += ";";
                }
            }
            $scope.iniValues.value = iniKeyValue;
            //function call to update a key on ini file
            $scope.editItem($scope.iniValues, destDetails);

            model.desFolderPath = destDetails.dTargetPath + "/" + $scope.value.dTargetFolder;
            //updating system_settings for *_retrieval_paths and *_retrieval_dbid
            webServices.postItem(getSystemSettingsUrl, model).success(function (response) {
                $scope.sysSettList = [];
                $scope.settingsPane = true;
                $scope.settings = response.settings;
                $scope.systemSettings = response.kvpList;
                $scope.settingsToShow = response.settingFromEntity;
                for (i = 0; i < $scope.settingsToShow.length; i++) {
                    for (j = 0; j < $scope.systemSettings.length; j++) {
                        if ($scope.settingsToShow[i].SETTING_NAME == $scope.systemSettings[j].Key) {
                            $scope.temp = {
                                "Key": $scope.systemSettings[j].Key,
                                "Value": $scope.systemSettings[j].Value
                            }
                            $scope.sysSettList[$scope.sysSettList.length] = $scope.temp;
                            break;
                        }
                    }
                }
                //toastr.success("Successfully loaded");
            }).error(function (error) {
                $scope.settingsPane = false;
                toastr.error("could not load settings");
            });
        }).error(function (error) {
            toastr.error("Couldn't load applications");
        }).finally(function () {
            $scope.viewBar = false;
            $scope.viewLoading = false;
            $scope.progressLoading = false;
            $scope.proBar = false;
        });
    }

    //initialize copying tables and views
    $scope.copyTable = function (model, sourceDetails, copyDB, destDetails) {
        $scope.copiedMessage = [];
        $scope.tableList = [];
        $scope.viewList = [];
        $scope.tableList2 = [];
        $scope.viewList2 = [];
        $scope.copyTableMsg = false;

        if (copyDB == "dvstream" || copyDB == "all") {
            if (copyDB == "dvstream") {
                model.desDbmName = null;
            }
            var div = document.getElementById("list");
            var tables = div.getElementsByTagName("p");
            for (var i = 0, j = 0; i < tables.length; ++i) {
                if (tables[i].childNodes[0].checked == true) {
                    $scope.tableList[j] = tables[i].childNodes[1].data;
                    j++;
                }
            }

            var div2 = document.getElementById("viewList");
            var views = div2.getElementsByTagName("p");
            for (var i = 0, j = 0; i < views.length; ++i) {
                if (views[i].childNodes[0].checked == true) {
                    $scope.viewList[j] = views[i].childNodes[1].data;
                    j++;
                }
            }
        }

        if (copyDB == "master" || copyDB == "all") {
            if (copyDB == "master") {
                model.desDvsName = null;
            }
            var div3 = document.getElementById("list2");
            var tables2 = div3.getElementsByTagName("p");
            for (var i = 0, j = 0; i < tables2.length; ++i) {
                if (tables2[i].childNodes[0].checked == true) {
                    $scope.tableList2[j] = tables2[i].childNodes[1].data;
                    j++;
                }
            }

            var div4 = document.getElementById("viewList2");
            var views2 = div4.getElementsByTagName("p");
            for (var i = 0, j = 0; i < views2.length; ++i) {
                if (views2[i].childNodes[0].checked == true) {
                    $scope.viewList2[j] = views2[i].childNodes[1].data;
                    j++;
                }
            }
        }

        if ($scope.tableList.length == 0 && $scope.viewList.length == 0 && $scope.tableList2.length == 0 && $scope.viewList2.length == 0) {
            alert("select atleast one table or view to continue.");
        }
        else {
            model.selectedTable = "";
            for (c = 0; c < $scope.tableList.length; c++) {
                model.selectedTable += $scope.tableList[c];
                if (c < $scope.tableList.length - 1) {
                    model.selectedTable += ";";
                }
            }

            model.selectedView = "";
            for (c = 0; c < $scope.viewList.length; c++) {
                model.selectedView += $scope.viewList[c];
                if (c < $scope.viewList.length - 1) {
                    model.selectedView += ";";
                }
            }

            model.selectedTable2 = "";
            for (c = 0; c < $scope.tableList2.length; c++) {
                model.selectedTable2 += $scope.tableList2[c];
                if (c < $scope.tableList2.length - 1) {
                    model.selectedTable2 += ";";
                }
            }

            model.selectedView2 = "";
            for (c = 0; c < $scope.viewList2.length; c++) {
                model.selectedView2 += $scope.viewList2[c];
                if (c < $scope.viewList2.length - 1) {
                    model.selectedView2 += ";";
                }
            }
            $scope.checkExists(model, sourceDetails, destDetails);
        }
    }

    //saving selected key from dropdown list to _clone_utility database
    $scope.sysSettList = [];
    $scope.selectedKey = function (key) {
        if (key != "") {
            webServices.postItem(addKeyToEntityUrl + "/" + key, "").error(function (error) {
                toastr.error("failed to add settingName to database");
            });
            for (i = 0; i < $scope.systemSettings.length; i++) {
                if (key == $scope.systemSettings[i].Key) {
                    $scope.temp = {
                        "Key" : $scope.systemSettings[i].Key,
                        "Value" : $scope.systemSettings[i].Value
                    }
                    $scope.sysSettList[$scope.sysSettList.length] = $scope.temp;
                    break;
                }
            }
        }
    }

    //Save settings table changes to destination's dvstream database
    $scope.saveSettings = function (model, sysSet) {
        var table = document.getElementById("sysSettingsTable");
        $scope.sysSet = [];
        for (var i = 1, row; row = table.rows[i]; i++) {
            var k, v;
            for (var j = 0, col; col = row.cells[j]; j++) {
                if (j == 0) {
                    k = col.innerText;
                } else if (j == 1) {
                    if (col.childNodes.length > 1) {
                        v = col.childNodes[1].value;
                    } else {
                        v = col.childNodes[0].value;
                    }
                }
            }
            $scope.temp = {
                "Key": k,
                "Value" : v
            }
            $scope.sysSet[i] = $scope.temp;
        }


        model.sysSettings = $scope.sysSet;
        webServices.putItem(systemSettingsUrl, model).success(function (response) {
            toastr.success("updated successfully");
            //refreshing the setting_values on UI
            $scope.sysSettList = [];
            $scope.systemSettings = response.kvpList;
            $scope.settingsToShow = response.settingFromEntity;
            for (i = 0; i < $scope.settingsToShow.length; i++) {
                for (j = 0; j < $scope.systemSettings.length; j++) {
                    if ($scope.settingsToShow[i].SETTING_NAME == $scope.systemSettings[j].Key) {
                        $scope.temp = {
                            "Key": $scope.systemSettings[j].Key,
                            "Value": $scope.systemSettings[j].Value
                        }
                        $scope.sysSettList[$scope.sysSettList.length] = $scope.temp;
                        break;
                    }
                }
            }
            toastr.info("setting values refreshed")
        }).error(function (error) {
            toastr.error("couldn't update settings");
        });
    }
//endOf database tab

//Log Tab
    $scope.refreshLog =function () {
            $scope.lines = {};
            webServices.postItem(systemSettingsUrl, "").success(function (response) {
                $scope.lines = response;
                $scope.myNumber = 10;
            });
        }
//endOf Log tab
}]);